def confidence(ante: str, post: str, supp: dict) -> int:
    print(
        f"{ante} => {post}: supp({ante}) = {supp[ante]}, "
        f"supp({''.join([ante, post])}) = {supp[''.join([ante, post])]}"
    )
    return round(supp[f"{ante}{post}"] / supp[ante] * 100)


if __name__ == "__main__":
    supp = {
        "A": 6,
        "B": 5,
        "C": 4,
        "D": 4,
        "AB": 3,
        "AC": 3,
        "AD": 3,
        "BC": 4,
        "BD": 4,
        "CD": 3,
        "ABD": 3,
        "BCD": 3,
    }

    ante = "B"
    post = "C"
    conf = confidence(ante, post, supp)
    print(conf)

    ante = "AB"
    post = "D"
    conf = confidence(ante, post, supp)
    print(conf)
