from typing import Sequence, Set, Tuple, Union

Vert = Union[int, str]
Weight = int
Edge = Tuple[Tuple[Vert, Vert], Weight]


def kruskal_mst_2(graph: Sequence[Edge]) -> Set[Edge]:
    edges = sorted(graph, key=lambda e: e[1])
    verts: Set[Vert] = set(edges[0][0])
    verts_tmp: Set[Tuple[Vert]] = set()

    mst: Set[Edge] = {edges[0]}

    for edge in edges[1:]:
        print(f"{edge=}")
        u, v = edge[0]

        if u in verts and v in verts:
            # Both vertices already in the MST
            print(f"\t{u} and {v} are already in {verts=}")
            continue
        elif u not in verts and v not in verts:
            # Edge belongs to another connected component
            mst.add(edge)
            verts_tmp.add((u, v))

            print(f"\t{u} and {v} added to {verts_tmp=}")
            print(f"\t{verts=}")
        else:
            # exactly one of {u, v} is in the MST
            print(f"\texactly one of {u}, {v} is in the MST; {verts=}, {verts_tmp=}")
            rmv = set()
            for conn_component in verts_tmp:
                # Append the components that are connected with the main tree
                # by this edge:
                print(f"\t{conn_component=}")
                if u in conn_component or v in conn_component:
                    mst.add(edge)
                    verts |= set(conn_component)
                    rmv.add(conn_component)
                    print(f"\tFound {u}: adding {edge=} to MST; {rmv=}, {verts_tmp=}")

                else:
                    print(f"\t{verts_tmp=}")
            verts_tmp -= rmv
            print(f"\tremoved {rmv=} -> {verts_tmp=}")
            # Add the edge even if it wasn't incident on any connected
            # components:
            mst.add(edge)
            verts |= {u, v}
            print(f"\t{u} and {v} added to the MST; {verts=}, {verts_tmp=}")
            continue
    return mst


if __name__ == "__main__":
    # graph = [
    #     (("a", "b"), 1),
    #     (("a", "e"), 7),
    #     (("b", "e"), 5),
    #     (("b", "c"), 4),
    #     (("b", "d"), 3),
    #     (("c", "d"), 2),
    #     (("e", "d"), 6),
    # ]
    graph = [
        ((1, 2), 4),
        ((2, 3), 24),
        ((3, 4), 9),
        ((4, 5), 7),
        ((1, 7), 6),
        ((3, 7), 23),
        ((6, 7), 5),
        ((3, 6), 18),
        ((4, 6), 11),
        ((1, 8), 16),
        ((7, 8), 8),
        ((6, 8), 10),
        ((5, 6), 14),
        ((5, 8), 21),
    ]
    # Now it seems to work; but try some more examples. And it could probably
    # be simplified and optimised.
    mst = kruskal_mst_2(graph)
    print(sorted(mst))

    tree_weight = sum(e[1] for e in mst)
    print(f"{tree_weight=}")
