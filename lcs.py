from typing import List, Tuple


def compute_lcs_tables(
    text_x: str, text_y: str
) -> Tuple[List[List[int]], List[List[tuple]]]:
    """The 'original' variant of LCS.

    Computes two matrices: c - storing the LCS lengths, and b - directions
    matrix, with coordinates of the previous cell where the result for the
    consecutive subproblem is stored.
    """
    m = len(text_y)
    n = len(text_x)
    c = [[0 for i in range(m + 1)] for j in range(n + 1)]
    b: List[List[tuple]] = [[() for i in range(m + 1)] for j in range(n + 1)]

    for i in range(1, n + 1):  # row index -> text_x chars
        for j in range(1, m + 1):  # column index -> text_y chars
            # Case 1: last characters of both strings match.
            if text_x[i - 1] == text_y[j - 1]:
                c[i][j] = c[i - 1][j - 1] + 1
                b[i][j] = (i - 1, j - 1)
            # Case 2: The last chars do not match;
            # LCS is longer when we drop the last char of text_x.
            elif c[i - 1][j] >= c[i][j - 1]:
                c[i][j] = c[i - 1][j]
                b[i][j] = (i - 1, j)
            # Case 3: The last chars do not match;
            # LCS is longer when we drop the last char of text_y.
            else:
                c[i][j] = c[i][j - 1]
                b[i][j] = (i, j - 1)
    return c, b


def print_lcs(b: List[List[Tuple[int, int]]], text_x: str, i: int, j: int) -> None:
    # i - row index (text_x chars); j - column index (text_y chars)
    if i == 0 or j == 0:
        return
    if b[i][j] == (i - 1, j - 1):
        print_lcs(b, text_x, i - 1, j - 1)
        print(f"({i}, {j}): {text_x[i - 1]}")
    elif b[i][j] == (i - 1, j):
        print_lcs(b, text_x, i - 1, j)
    else:
        print_lcs(b, text_x, i, j - 1)


def lcs_dp_original(text_x: str, text_y: str) -> None:
    c, b = compute_lcs_tables(text_x, text_y)
    print("Length of LCS:")
    print("     ", "  ".join(text_y))
    for t, row_c in zip("-" + text_x, c):
        print(t, row_c)

    print("LCS:")
    print_lcs(b, text_x, len(text_x), len(text_y))

    print("Direction matrix b:")
    print("\t", "\t".join(text_y))
    for t, row_b in zip("-" + text_x, b):
        print(t, row_b)


def lcs_2rows(text_x: str, text_y: str) -> str:
    """Compute LCS of two texts, only storing 2 rows of the LCS matrix."""
    # Use the shorter of the texts as rows to reduce the size of the stored data.
    col_text, row_text = sorted([text_x, text_y], key=lambda t: len(t))

    # Prepare the two rows which we need
    prev = [""] + ["" for _ in col_text]  # additional 0-th element at the start
    curr = [""]

    for i in range(1, len(row_text) + 1):
        for j in range(1, len(col_text) + 1):
            # Case 1: last characters of both strings match.
            if row_text[i - 1] == col_text[j - 1]:
                curr.append(prev[j - 1] + col_text[j - 1])
            # Case 2: last chars do not match, the LCS is longer
            # if we drop the last char of the row_text.
            elif len(prev[j]) >= len(curr[j - 1]):
                curr.append(prev[j])
            # Case 3: last chars do not match, the LCS is longer
            # if we drop the last char of the col_text.
            else:
                curr.append(curr[j - 1])

        prev = curr
        curr = [""]

    return curr[-1]


if __name__ == "__main__":
    # text_x = "ACGCTAC"
    # text_y = "CTGACA"

    # text_x = "PARMA"
    # text_y = "PANAMA"

    # text_x = "AGGTAB"
    # text_y = "GXTXAYB"

    text_x = "TARANTULA"
    text_y = text_x[::-1]

    lcs_dp_original(text_x, text_y)
    lcs_2rows(text_x, text_y)
