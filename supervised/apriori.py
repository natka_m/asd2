from collections import Counter, defaultdict
from functools import reduce
from itertools import combinations
from pprint import pprint
from typing import Dict, List, Optional


def join_candidates(values) -> List:
    return reduce(lambda x, y: list(x) + list(y), values)


def apriori(
    db: dict, min_supp: int, min_conf: Optional[int] = None
) -> Dict[frozenset, int]:
    db = {k: set(v) for k, v in db.items()}
    # db = dict(sorted(db.items(), key=lambda i: i[1]))

    k = 1
    c_1 = Counter(join_candidates(db.values()))

    l_1 = {frozenset(k): v for k, v in c_1.items() if v >= min_supp}
    l_i = l_1
    print(f"{k=}:")
    pprint(f"{c_1=}")
    pprint(f"{l_1=}")
    print("")

    supp_count: Dict[frozenset, int] = defaultdict(int)
    supp_count.update(l_i)

    while l_i:
        k += 1
        items = reduce(lambda x, y: x | y, l_i.keys())
        c_2 = [
            frozenset(c)
            for c in combinations(items, k)
            if all(frozenset(s) <= items for s in c)
        ]
        c_2_counter = {c: len([t for t in db.values() if c <= t]) for c in c_2}
        # TODO: The c_{i} contains more item combinations than in the optimal
        # version; for k=3, c_3 should not contain e.g. ABE or AEF (AB and AF
        # are not in L_2 (c_2_pruned)!)
        c_2_pruned = {c: v for c, v in c_2_counter.items() if v >= min_supp}
        print(f"{k=}:")
        pprint(f"{c_2=}")
        pprint(f"{c_2_pruned=}")
        print("")

        if not c_2_pruned:
            break
        else:
            l_i = c_2_pruned
            supp_count.update(l_i)
            # print(f"{supp_count=}")

    return supp_count


if __name__ == "__main__":
    # db = {  # g4g test
    #     1: "125",
    #     2: "24",
    #     3: "23",
    #     4: "124",
    #     5: "13",
    #     6: "23",
    #     7: "13",
    #     8: "1235",
    #     9: "123",
    # }

    # min_supp = 2
    # # min_conf = 0.5

    db = {
        1: "BG",
        2: "BCE",
        3: "AEF",
        4: "BEF",
        5: "AEG",
        6: "BDFG",
        7: "BFG",
        8: "AEG",
    }
    min_supp = 2
    # min_conf = ...

    supp_count = apriori(db, min_supp)
    supp_itemsets_as_str = {"".join(sorted(k)): v for k, v in supp_count.items()}
    supp_sorted = sorted(supp_itemsets_as_str.items(), key=lambda i: (len(i[0]), i[0]))
    pprint(supp_sorted)
