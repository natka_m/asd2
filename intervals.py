from typing import List, Tuple


def schedule(intervals: List[Tuple[int, int]]) -> List[Tuple[int, int]]:
    """Greedily schedule the maximum number of non-overlapping intervals."""
    # Sort the intervals by their finish time, non-decreasing:
    ints = sorted(intervals, key=lambda i: i[1])
    result = [ints[0]]
    last = ints[0][1]
    for i in ints:
        if i[0] >= last:
            result.append(i)
            last = i[1]
    return result


def schedule_rev(intervals: List[Tuple[int, int]]) -> List[Tuple[int, int]]:
    """Greedily schedule the maximum number of non-overlapping intervals."""
    # Sort the intervals by their start time, non-increasing:
    ints = sorted(intervals, key=lambda i: i[0], reverse=True)
    result = [ints[0]]
    earliest = ints[0][0]
    for i in ints:
        if i[1] <= earliest:
            result.append(i)
            earliest = i[0]
    return result


if __name__ == "__main__":
    intervals = [
        (15, 36),
        (42, 61),
        (0, 18),
        (80, 100),
        (12, 28),
        (50, 54),
        (40, 69),
        (66, 80),
        (20, 27),
        (30, 44),
    ]

    result = schedule(intervals)
    print(result)
    result = schedule_rev(intervals)
    print(result)
