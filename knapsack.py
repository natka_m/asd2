from typing import Any, Sequence, List, Set


def solve(values: List[int], costs: List[int], max_cost: int) -> List[List[int]]:
    """Calculate the table of maximal total values."""
    assert len(values) == len(
        costs
    ), "Lengths of values and costs arrays have to match!"
    c = [[0 for p in range(max_cost + 1)] for item in (values + [1])]

    for i in range(len(values) + 1):
        for avail_money in range(max_cost + 1):
            if i == 0 or avail_money == 0:
                continue

            tmp_val = 0

            # Note that the costs' and values' need to be shifted back by one,
            # because we've added the 0th column to the tables:
            cost = costs[i - 1]
            val = values[i - 1]
            # Can we buy item i?
            if cost <= avail_money:
                # Add item i, and if there's still money left, add the items we
                # can get for it (money left = avail_money - cost)
                tmp_val = val + c[i - 1][avail_money - cost]
            # It may be better not to buy the i-th item, however:
            if tmp_val < c[i - 1][avail_money]:
                tmp_val = c[i - 1][avail_money]

            c[i][avail_money] = tmp_val

    print_table(c)

    return c


def get_items(
    c: List[List[int]], costs: List[int], i: int, avail_money: int
) -> Set[int]:
    """Retrieve the purchased items from the max. total values table ``c``."""
    # Base case: no items
    if i == 0:
        return set()
    # Either we bought item i, given the available sum of money...
    if c[i][avail_money] > c[i - 1][avail_money]:
        return get_items(c, costs, i - 1, avail_money - costs[i - 1]) | set([i])
    # ...or, if the solution without item i was better:
    return get_items(c, costs, i - 1, avail_money)


def print_table(c: Sequence[Sequence[Any]]) -> None:
    for i, _ in enumerate(c[0]):
        print(f"\t{i}", end="")
    for i, row in enumerate(c):
        print(f"\nItem {i}\t", end=" ")
        for col in row:
            print(f"{col}\t", end="")
    print()


if __name__ == "__main__":
    ## Example from the hackerearth article:
    # values = [10, 40, 30, 50]
    # costs = [5, 4, 6, 3]
    # max_cost = 10

    ## Example from wikipedia:
    # values = [505, 352, 458, 220, 354, 414, 498, 545, 473, 543]
    # costs = [23, 26, 20, 18, 32, 27, 29, 26, 30, 27]
    # max_cost = 67

    ## Example from the assignment:
    values = [6, 12, 4, 16, 10]
    costs = [3, 5, 2, 8, 6]
    max_cost = 14

    ## Made-up example with more objects than euros:
    values = [6, 4, 2, 1, 3, 8, 5, 4, 3, 2, 2, 1, 1]
    costs = [1, 2, 1, 1, 1, 5, 1, 1, 4, 2, 1, 1, 3]
    max_cost = 6

    c = solve(values, costs, max_cost)
    total_val = c[-1][-1]
    print(total_val)
    print(get_items(c, costs, len(costs), max_cost))
